package cn.hdu.zkh.web.controller;

import cn.hdu.zkh.domain.User;
import cn.hdu.zkh.service.UserService;
import com.dxy.commons.beans.ResponseDataholder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by liuyumeng on 17-5-20.
 */
@RestController
public class UserController {
    @Resource
    private UserService userService;
    @RequestMapping("/japi/getAllUser")
    public ResponseDataholder getAllUser(HttpServletRequest request){
        List<User> userList = userService.getAllUser();
        return ResponseDataholder.success().setItems(userList);
    }
}
