package cn.hdu.zkh.dao.daoImpl;

import com.dxy.commons.dao.BaseDAOImpl;
import com.dxy.commons.dao.CanonicalDomain;
import org.hibernate.SessionFactory;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * Created by liuyumeng on 17-5-7.
 */
public class MyBaseDaoImpl<V extends CanonicalDomain<K>,K extends Serializable> extends BaseDAOImpl<V, K> {
    @Resource(name = "wxpubSessionFactory")
    public void setSessionFactory0(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}
