package cn.hdu.zkh.service.impl;

import cn.hdu.zkh.dao.UserDAO;
import cn.hdu.zkh.domain.User;
import cn.hdu.zkh.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by liuyumeng on 17-5-20.
 */
@Service
public class UserServiceImpl implements UserService{
     @Resource
     private UserDAO userDAO;
     @Override
     public List<User> getAllUser() {
          return userDAO.getAll();
     }
}
