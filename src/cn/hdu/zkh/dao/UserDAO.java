package cn.hdu.zkh.dao;

import cn.hdu.zkh.domain.User;
import com.dxy.commons.dao.BaseDAO;

/**
 * Created by liuyumeng on 17-5-20.
 */
public interface UserDAO extends BaseDAO<User,Integer>{
}
