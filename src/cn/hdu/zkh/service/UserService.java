package cn.hdu.zkh.service;

import cn.hdu.zkh.domain.User;

import java.util.List;

/**
 * Created by liuyumeng on 17-5-20.
 */
public interface UserService {
     List<User> getAllUser();
}
