package cn.hdu.zkh.domain;

import com.dxy.commons.dao.CanonicalDomain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by liuyumeng on 17-5-20.
 */
@Entity
@Table
public class User implements CanonicalDomain<Integer>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String username;
    private String password;
    private String createTime;
    private String modifyTime;

    @Override
    public void prepare() {

    }

    @Override
    public Date getCreateTime() {
        return null;
    }

    @Override
    public void setCreateTime(Date date) {

    }

    @Override
    public Date getModifyTime() {
        return null;
    }

    @Override
    public void setModifyTime(Date date) {

    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
