package cn.hdu.zkh.dao.daoImpl;

import cn.hdu.zkh.dao.UserDAO;
import cn.hdu.zkh.domain.User;
import org.springframework.stereotype.Repository;

/**
 * Created by liuyumeng on 17-5-20.
 */
@Repository
public class UserDAOImpl  extends  MyBaseDaoImpl<User,Integer> implements UserDAO{
}
